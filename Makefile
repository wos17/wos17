# Path to cross tools

MIPS_PREFIX=mips-sde-elf

EXECUTABLES=$(addprefix bin/, kernel)


# gcc flags for the MIPS architecture:
#  -EL     : Little endian
#  -G0     : Always use small data (using register gp)
#  -mips32 : Compile for a 32-bit MIPS architecture
#

ARCH=-EL -G0 -mips32

# Other gcc flags

CFLAGS	+= -ggdb -Wall -fno-builtin -std=c99 -I include

# Compiler and linker commands

CC=$(MIPS_PREFIX)-gcc

# Tell the linker where to start the text segment. 
LD=$(MIPS_PREFIX)-ld -Ttext 80020000


# Path to Simics installation

SIMICS=$(HOME)/simics-workspace 




#### RULE USED TO START SIMICS 

do_boot_kernel: bin/kernel 
	./scripts/run.sh $(SIMICS) $<

#### RULES TO BUILD BINARIES FROM OBJECT FILES



bin/kernel: $(addprefix build/, kernel.o boot.o debug.o uart.o lcd.o scheduler.o pcb.o ipc.o syscall.o kmalloc.o printf.o sem.o rand.o demo_sync.o demo_msg.o shell.o timer.o clock.o)

	$(LD) $(ARCH) -o $@ $^

#bin/boot_tty3: $(addprefix build/, boot_tty2.o tty3.o)
#	$(LD) $(ARCH) -o $@ $^

#bin/boot_tty%: $(addprefix build/, boot_tty%.o tty%.o)
#	$(LD) $(ARCH) -o $@ $^

#### Add dependency on headerfile of various tty.o files

#build/tty%.o: tty%.c include/tty%.h
#	$(CC) $(ARCH) $(CFLAGS)  -c $< -o $@	


###### GENERIC BUILD PATTERNS ########

build/%.o: src/%.c
	$(CC) $(ARCH) $(CFLAGS)  -c $< -o $@	

build/%.o: src/%.S
	$(CC) $(ARCH) $(CFLAGS)  -c $< -o $@

clean: 
	pwd
	rm -f build/*.o
	rm -f include/*~ include/#* include/*#
	rm -f src/*~ src/#* src/*#
	rm -f scripts/*~ scripts/#* scripts/*#
	rm -f ${EXECUTABLES}	
