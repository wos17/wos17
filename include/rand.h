/*! \file rand.h
    \brief A simple pseudo rng based on POSIX exapmle
    
    A simple pseudo rng based on POSIX example
    http://linux.die.net/man/3/rand
*/
#ifndef __rand_h__
#define __rand_h__

#include "types.h"



uint32_t rand(uint32_t *seed, uint32_t max);
uint32_t xor128(void);

#endif
