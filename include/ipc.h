#ifndef __IPC_H__
#define __IPC_H__

#include "scheduler.h"
#include "kmalloc.h"
#include "pcb.h"


/*
#	type used for message/pointer passing.
#	contains pointer to next msg in queue, pointer to data and an int to flag actions.
*/
//Codes used by ctrl-field in message element (mail == *struct msg)
#define WAIT_SEM 0
#define SIGNAL_SEM 1

mail_t mk_mail(uint32_t data, uint32_t ctrl);
void push_mail(mail_t m, pcb_t *p);
void rm_mail(mail_t msg);

#endif
