/*! \file timer.h
    \brief Header for timer program
    
    Header for timer program
*/

#include "lcd.h"
#include "types.h"
#include "syscall.h"

void timer_start();
void blink();
void answer();
void russianroulette();
