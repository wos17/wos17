/*! \file pcb.h
    \brief Defines the PCB
    
    Types and functions for working with the PCB and lists of PCBs
*/
#ifndef __PCB_H__
#define __PCB_H__

#include "registers.h"
#include "sem.h"
#include "stack.h"


//Pointerpassing ipc-elements
struct _msg {
	struct _msg *next;
	uint32_t data;
	uint32_t ctrl;
};

typedef struct _msg msg_t;
typedef msg_t* mail_t;

// http://en.wikipedia.org/wiki/MIPS_architecture#Compiler_register_usage
/* REGISTERS
	0	zero		Always equal to zero
	1	at		Assembler temporary; used by the assembler
	2-3 	v0-v1		Return value from function call
	4-7	a0-a3		First four parameters for a function call
	8-15	t0-t7		Temporary variables
	16-23	s0-s7		Saved variables
	24-25	t8-t9		Two more temporary variables
	26-27	k0-k1		Kernel use registers
	28	gp		Global pointer
	29	sp		Stack pointer
	30	fp/s8 		Stack frame pointer or subroutine variable
	31	ra		Return address of the last call
*/

/*! \struct pcb
    \brief Defines the PCB
*/
struct pcb {
	uint32_t id;			/**< id of the process */
	uint32_t pid;			/**< id of the parent of the process */
	uint32_t status;		/**< status of the process, valid statuses in scheduler.h */
	uint32_t exit_status;	/**< exit status of the process after exit and before parent collects it*/
	uint32_t wait_child;	/**< id of the child the process is waiting for if status is STAT_WAITING and
								the process is in wait_list */
	mail_t mail;			/**< the mailbox of the process */
	mail_t wait_mail;		/**< buffer when waiting for mail */
	uint32_t tty;			/**< tty for input and output for the process. DEV_TTY0 or DEV_TTY1 */
	struct pcb *next;		/**< If the process is in a list this holds the address of the next element */
	char *buf;				/**< If status is STAT_WAIT_IO and the process is in wait_io_list this holds
								the address of the input buffer for the process */
	uint32_t time;			/**< If status STAT_WAIT_TIME and the process is in wait_time_list time holds
							      the number of milliseconds until it should be set to STAT_READY and moved
							      to the ready list. */
    sem_t *sem;				/**< if status is STAT_WAIT_SEM and the process is in the wait_sem_list sem holds
								the address of the semaphore (sem_t) it is waiting for. */
	char name[32];			/**< the name of the process. For when processes are printed. */
	registers_t registers; 	/**< Storage space for the registers in context switches. Defined in registers.h. */
	stack_t stack;			/**< Stack space for the process. Defined in stack.h. */
};

/*! \var pcb_t
    \brief type for the pcb struct
*/
typedef struct pcb pcb_t;

/*! \struct pcb_list
    \brief A list of PCBs
*/
typedef struct {
	pcb_t *first;			/**< Adress of the first element or 0 */
	uint32_t length;		/**< Number of elements in the list */
} pcb_list;

/* \var (*cmp_func)
   \brief Typedef for compare function to use in s_insert();
*/
typedef int32_t (*cmp_func)(pcb_t *a, pcb_t *b);

void insert(pcb_list *plist, pcb_t *proc);
void s_insert(pcb_list *plist, pcb_t *proc, cmp_func compare);
pcb_t* remove(pcb_list *plist, pcb_t *proc);
pcb_t* remove_id_cid(pcb_list *plist, uint32_t id, uint32_t cid);
pcb_t* remove_id(pcb_list *plist, uint32_t pid);
pcb_t* pop(pcb_list *plist);
void push(pcb_list *plist, pcb_t *proc);
pcb_t* remove_pid(pcb_list *plist, uint32_t pid);
pcb_t* remove_tty(pcb_list *plist, uint32_t tty);
pcb_t* search_tty(pcb_list *plist, uint32_t tty);
#endif
