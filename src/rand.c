/*! \file rand.c
    \brief A simple pseudo rng based on POSIX exapmle
    
    A simple pseudo rng based on POSIX example
    http://linux.die.net/man/3/rand
*/
#include "rand.h"
#include "clock.h"
//! Pseudo random number generator
/*! Generates "random" numbers based on seed, state of the generator will be
    saved in *seed between runs.
    \param seed Pointer to uint32_t with initial seed. State will be saved here.
    \param max Upper limit of range.
 */
uint32_t rand(uint32_t *seed, uint32_t max) {
    *seed = *seed * 1103515245 + 12345;
    return((uint32_t)(*seed/65536) % max);
}

uint32_t xor128(void) {
  uint32_t x = curr_time();
  uint32_t y = 362436069;
  uint32_t z = 521288629;
  uint32_t w = 88675123;
  uint32_t t;
 
  t = x ^ (x << 11);
  x = y; y = z; z = w;
  return w = w ^ (w >> 19) ^ (t ^ (t >> 8));
}
