/*! \file demo_sync.c
    \brief A demo program showing off semaphores
    
    A simple demo program meant to demonstate synchronization
    of processes with the help of semaphores
*/

#include "demo_sync.h"
#include "syscall.h"
#include "sem.h"
#include "rand.h"

void rendezvous(sem_t *s1, sem_t *s2);

/*! \brief Entry point of demo_sync
    
    This program creates two processes which synchronize their output with
    the help of two semaphores. It then exits.
*/

void sync_main() {
	int32_t proc1 = 0;
	int32_t proc2 = 0;
	sem_t *sem1 = (sem_t*)malloc(sizeof(sem_t));
	sem_t *sem2 = (sem_t*)malloc(sizeof(sem_t));
	sem_init(sem1, 1);
	sem_init(sem2, 1);
	proc1 = fork();
	switch(proc1) {
	case -1:
		printf("Fork failed!\n");
		exit(666);
		break;
	case 0:
		rendezvous(sem1, sem2);
		break;
	default:
		printf("Child 1 sent off with id: %d\n", proc1);
	}

	proc2 = fork();
	switch(proc2) {
	case -1:
		printf("Fork failed!\n");
		exit(666);
		break;
	case 0:
		rendezvous(sem2, sem1);
		break;
	default:
		printf("Child 2 sent off with id: %d\n", proc2);
	}

	uint32_t status = 0;
	waitid(&status, proc2);
	printf("Child 2 returned happily!\n");
	
	waitid(&status, proc1);
	printf("Child 1 returned happily!\n");
	exit(0);

	return;
}
/*! \brief Syncronizes output between two processes with the help of semaphores
    
    This function is meant to be run by two processes at the same time, they must be
    called with the two semaphores in reversed place in argument list. It then exits.
	\param s1 Semaphore 1. Initialized to 1.
	\param s2 Semaphore 2. Initialized to 1.
	\return void.
*/
void rendezvous(sem_t *s1, sem_t *s2) {
	uint32_t id = getid();
	uint32_t seed = id;
	for(int i = 1; i < 11; i++) {
		sem_wait(s1);
		printf("Process %d> %d\n", id, i);
		sleep(rand(&seed, 10000));
		sem_signal(s2);
	}
	printf("Process %d> Exiting\n", id);
	exit(0);

	return;
}
