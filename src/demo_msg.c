#include "demo_msg.h"
#include "syscall.h"

#define NUM_CHILD 6
#define MSG_PRINT 0
#define MSG_DIE 1

void demo_msg_main() {
	int32_t children[NUM_CHILD];
	uint32_t id = getid();
	int cid = 0;
	uint32_t retstat = 0;
	//Spawn a number of childern
	for(int i = 0; i < NUM_CHILD; i++) {
		cid = fork();
		if(cid == 0) {
			exec(&child_process, "child");
		} else if(cid == -1) {
			printf("Fork failed\n");
			exit(666);
		} else {
			printf("Parent %d> Spawned child %d\n", id, cid);
			children[i] = cid;
		}
	}
	printprocs();
	for(int i = 0; i < NUM_CHILD; i++) {
		sleep(5000);
		printf("Parent %d> Sending message print to %d.\n", id, children[i]);
		send(i+10, MSG_PRINT, children[i]);
	}

	for(int i = 0; i < NUM_CHILD; i++) {
		sleep(3000);
		printf("Parent %d> Sending message die to %d.\n", id, children[i]);
		send(0, MSG_DIE, children[i]);
	}

	for(int i = 0; i < NUM_CHILD; i++) {
		cid = wait(&retstat);
		printf("Parent %d> Collected child %d\n", id, cid);
	}
	exit(0);
}


void child_process() {
	uint32_t id = getid();
	mail_t msg = (mail_t)malloc(sizeof(msg_t));
	printf("Process %d> Waiting for message.\n", id);
	while(1) {
		receive_wait(msg);
		if(msg->ctrl == MSG_PRINT) {
			printf("Process %d> Received %d.\n", id, msg->data);
		} else if (msg->ctrl == MSG_DIE) {
			printf("Process %d> Ordered to die.\n", id);
			free(msg);
			exit(0);
		}
	}

	exit(0);
}
