#include <stdarg.h> 			//needed for variadic functions
#include "syscall.h"
#include "asm.h"
#include "uart.h"

#define OUTPUT_BUFFER_SIZE 128 	//buffer size for printf

/* Helper function for printf, converts from integer to ascii
 * 1st arg is output buffer
 * 2nd arg is integer to convert
 * 3rd arg is base to print result in
 * Returns number of chars put in output buffer
 */
int myitoa(char* output, uint32_t n, int base) {
	int mult = 1;
	int chars = 0;
	int bin_count = 1;
	if(n < 0 && base == 10) {
		n *= -1;
		*output++ = '-';
		chars++;
	}
	while(n/mult >= base) {
		mult *= base;
		bin_count++;
	}
	if(base == 2) {
		int i = bin_count;
		i %= 4;
		i = 4 - i;
		chars += i;
		bin_count = i;
		while(i) {
			*output++ = '0';
			i--;
		}
	}
	while(mult != 0) {
		if(base == 2 && bin_count == 4) {
			*output++ = ' ';
			chars++;
			bin_count = 0;
		}
		int out_char = n / mult;
		n %= mult;
		mult /= base;
		if(out_char < 10) {
			*output++ = out_char + '0';
		} else {
			*output++ = out_char + 'A' - 10;
		}
		bin_count++;
		chars++;
	}
	return chars;
}

/* My very bad implementation of printf
 * Accepts %d, %b, %x, %c and %s in format string
 * %d, integer - prints as decimal number
 * %b, integer - prints as binary number
 * %x, integer - prints as hexadecimal number
 * 
 * %c, char - prints as char
 * %s, string - prints as string
 */

void printf(char* line, ...) {

	char output[OUTPUT_BUFFER_SIZE];	//Output buffer
	int oi = 0;							//Output index
	char *in_pt = line;					//Input index
	va_list arg_list;					//Extra arguments
	va_start(arg_list, line);			//Init extra arg list

	while(*in_pt != '\0') {			
		if(*in_pt != '%') {
			output[oi++] = *in_pt;
			in_pt++;
		} else if(*(in_pt+1) != 'd' &&
				  *(in_pt+1) != 'c' &&
				  *(in_pt+1) != 'x' &&
				  *(in_pt+1) != 'b' &&
				  *(in_pt+1) != 's')
		{
			output[oi++] = *in_pt;
			in_pt++;
			output[oi++] = *in_pt;
			in_pt++;				
		} else {
			in_pt++;
			if(*in_pt == 'd') {
				oi += myitoa(&output[oi], va_arg(arg_list, int), 10);
			}
			if(*in_pt == 'c') {
				char c = (char)va_arg(arg_list, int);
				output[oi++] = c;
			}
			if(*in_pt == 'x') {
				output[oi++] = '0';
				output[oi++] = 'x';
				oi += myitoa(&output[oi], va_arg(arg_list, uint32_t), 16);
			}
			if(*in_pt == 's') {
				char* arg_string = va_arg(arg_list, char*);
				while(*arg_string != '\0') {
					output[oi++] = *arg_string++;
				}
			}
			if(*in_pt == 'b') {
				oi += myitoa(&output[oi], va_arg(arg_list, int), 2);
			}
			in_pt++;
		}
	}
	va_end(arg_list);
	output[oi] = '\0';
	ksyscall((uint32_t) SC_PRINTLINE, (uint32_t) output, 0, 0);
}

void debug_printf(char* line, ...) {

	char output[OUTPUT_BUFFER_SIZE];	//Output buffer
	int oi = 0;							//Output index
	char *in_pt = line;					//Input index
	va_list arg_list;					//Extra arguments
	va_start(arg_list, line);			//Init extra arg list

	while(*in_pt != '\0') {			
		if(*in_pt != '%') {
			output[oi++] = *in_pt;
			in_pt++;
		} else if(*(in_pt+1) != 'd' &&
				  *(in_pt+1) != 'c' &&
				  *(in_pt+1) != 'x' &&
				  *(in_pt+1) != 'b' &&
				  *(in_pt+1) != 's')
		{
			output[oi++] = *in_pt;
			in_pt++;
			output[oi++] = *in_pt;
			in_pt++;				
		} else {
			in_pt++;
			if(*in_pt == 'd') {
				oi += myitoa(&output[oi], va_arg(arg_list, int), 10);
			}
			if(*in_pt == 'c') {
				char c = (char)va_arg(arg_list, int);
				output[oi++] = c;
			}
			if(*in_pt == 'x') {
				output[oi++] = '0';
				output[oi++] = 'x';
				oi += myitoa(&output[oi], va_arg(arg_list, uint32_t), 16);
			}
			if(*in_pt == 's') {
				char* arg_string = va_arg(arg_list, char*);
				while(*arg_string != '\0') {
					output[oi++] = *arg_string++;
				}
			}
			if(*in_pt == 'b') {
				oi += myitoa(&output[oi], va_arg(arg_list, int), 2);
			}
			in_pt++;
		}
	}
	va_end(arg_list);
	output[oi] = '\0';
	printstr(output);
}

