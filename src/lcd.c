#include "malta.h"

void init_lcd() {
  /* nothing to init */
}

/* display_word:
 *   Display a value on the Malta display.
 */
void display_word(uint32_t word)
{
  int i;
  //malta->ledbar.reg = 0xFF;
  for (i = 7; i >= 0; --i) {
    malta->asciipos[i].value = '0' + word % 10;
    word /= 10;
  }
}
